#include <catch2/catch_all.hpp>

#include "utils/manifest_parser.hpp"

TEST_CASE("Manifest parser", "[manifestparser]")
{
    QByteArray content = R"content(<?xml version='1.0' encoding='UTF-8'?>
<PoBVersion>
    <Version number="2.6.0" />
    <Source part="default" url="https://raw.githubusercontent.com/PathOfBuildingCommunity/PathOfBuilding/{branch}/" />
    <Source part="program" url="https://raw.githubusercontent.com/PathOfBuildingCommunity/PathOfBuilding/{branch}/src/" />
    <Source part="tree" url="https://raw.githubusercontent.com/PathOfBuildingCommunity/PathOfBuilding/{branch}/src/" />
    <File name="LICENSE.md" part="default" sha1="eacedd00e9707defe051bd7151466d223376afa6" />
    <File name="dummy0" part="program" sha1="eacedd00e9707defe051bd7151466d223376afa6" />
    <File name="dummy1" part="tree" sha1="eacedd00e9707defe051bd7151466d223376afa6" />
</PoBVersion>)content";
    auto result = ManifestParser::parse(content);
    auto expected_sha1 = QByteArray::fromHex("eacedd00e9707defe051bd7151466d223376afa6");
    REQUIRE(result._version == "2.6.0");
    REQUIRE(result._sources.size() == 3);
    REQUIRE(result._files.count("default") == 1);
    REQUIRE(result._files["default"].size() == 1);
    REQUIRE(result._files["default"].front()._path == "LICENSE.md");
    REQUIRE(result._files["default"].front()._sha1 == expected_sha1);
}
