#pragma once

#include <QDir>
#include <QList>
#include <QNetworkAccessManager>
#include <QString>

#include <fmt/format.h>

struct Context
{
    QNetworkAccessManager _qnam;
    const QDir _scratch;
    const QList<QString> _parts;
    const unsigned _verbose_level = 0;
    const bool _dry_run = false;

    template<typename... T>
    void logVerbose(unsigned level, fmt::format_string<T...> f, const T &... a)
    {
        if (level <= _verbose_level) {
            printErr(fmt::format(f, std::forward<const T &>(a)...));
        }
    }

    void printErr(const std::string &s);

    Context(Context const &) = delete;
    Context &operator=(Context &) = delete;
};
