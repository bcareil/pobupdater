#pragma once

#include <QString>

#include <fmt/format.h>

template<>
struct fmt::formatter<QString>
{
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin())
    {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') {
            throw format_error("invalid format");
        }
        return it;
    }

    template<typename FormatContext>
    auto format(const QString &s, FormatContext &ctx) -> decltype(ctx.out())
    {
        return format_to(ctx.out(), "{}", s.toUtf8().data());
    }
};

template<>
struct fmt::formatter<QStringRef>
{
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin())
    {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') {
            throw format_error("invalid format");
        }
        return it;
    }

    template<typename FormatContext>
    auto format(const QStringRef &s, FormatContext &ctx) -> decltype(ctx.out())
    {
        return format_to(ctx.out(), "{}", s.toUtf8().data());
    }
};
