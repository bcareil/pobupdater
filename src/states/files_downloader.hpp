#ifndef FILESDOWNLOADDER_HPP
#define FILESDOWNLOADDER_HPP

#include <QObject>

#include "../fmt-qt/string.hpp"
#include "../context.hpp"
#include "../utils/manifest_content.hpp"
#include "../utils/expected_file.hpp"
#include "../utils/files_checker.hpp"
#include "../utils/file_downloader.hpp"

struct DeleteLater
{
    void operator()(QObject *p)
    {
        if (p)
            p->deleteLater();
    }
};

class FilesDownloader : public QObject
{
    Q_OBJECT
private:
    using FileDownloaderPtr = std::unique_ptr<FileDownloader, DeleteLater>;

public:
    static constexpr size_t MaxConcurrentDownloads = 2;

public:
    explicit FilesDownloader(QObject *parent, Context &ctx, ManifestContent manifest);

signals:
    void allFilesDownloaded(QList<DownloadedFile> downloaded_files);
    void downloadError(std::string message);

private Q_SLOTS:
    void onFileCheckedOk(ExpectedFile file);
    void onFileCheckFailed(ExpectedFile file, FilesChecker::Reason reason);
    void onFileCheckError(std::string message);
    void onFileCheckCompleted();
    void onDownloadError(std::string message);
    void onDownloadSuccess(QByteArray data, bool is_binary);

private:
    void tryStartDownload();
    void tryComplete();

private:
    Context &_ctx;
    ManifestContent _manifest;
    FilesChecker _files_checker;
    std::vector<ExpectedFile> _download_queue;
    std::vector<std::pair<FileDownloaderPtr, ExpectedFile>> _downloaders;
    QList<DownloadedFile> _downloaded_files;
};

#endif // FILESDOWNLOADDER_HPP
