#include "files_downloader.hpp"

namespace {

constexpr std::string_view reasonToString(FilesChecker::Reason reason)
{
    switch (reason) {
    case FilesChecker::Reason::ChecksumMismatch:
        return "checksum mismatches";
    case FilesChecker::Reason::FileMissing:
        return "file missing";
    }
    return "<unknown>";
}

bool writeFile(QFile &fd, const QByteArray &data, bool is_binary)
{
    if (is_binary) {
        if (fd.write(data) != data.size()) {
            return false;
        }
    } else {
        // we have to convert LF to CRLF
        int prev_idx = 0;
        while (true) {
            int lf_idx = data.indexOf('\n', prev_idx + 1);
            if (lf_idx < 0) {
                lf_idx = data.size();
            }

            fd.write(&data.data()[prev_idx], lf_idx - prev_idx);
            if (lf_idx == data.size()) {
                break;
            }
            if (fd.error()) {
                return false;
            }

            if (lf_idx > 0 && data[lf_idx - 1] != '\r') {
                fd.write("\r", 1);
                if (fd.error()) {
                    return false;
                }
            }
            prev_idx = lf_idx;
        }
    }
    if (fd.error() || not fd.flush()) {
        return false;
    }
    return true;
}

QByteArray hashFile(QFile &fd)
{
    QCryptographicHash hasher(QCryptographicHash::Sha1);
    std::array<char, 4096> buffer;
    while (not fd.atEnd()) {
        auto n_read = fd.read(buffer.data(), buffer.size());
        if (n_read < 0 || fd.error()) {
            return {};
        }

        hasher.addData(buffer.data(), n_read);
    }
    return hasher.result();
}

} // end of anonymous namespace

FilesDownloader::FilesDownloader(QObject *parent, Context &ctx, ManifestContent manifest)
    : QObject(parent), _ctx(ctx), _manifest(std::move(manifest)), _files_checker(this, ctx)
{
    std::vector<ExpectedFile> expected_files;
    expected_files.reserve(
            std::accumulate(begin(_manifest._files), end(_manifest._files), 0UL,
                            [](size_t a, const auto &e) { return e.second.size() + a; }));

    for (const auto &files : _manifest._files) {
        QString source_name(files.first.c_str());
        if (not ctx._parts.contains(source_name)) {
            continue;
        }
        auto source = _manifest._sources.find(files.first);
        if (source == end(_manifest._sources)) {
            throw std::runtime_error(
                    fmt::format("Source {} present in files but not in sources list", files.first));
        }
        for (const auto &file : files.second) {
            expected_files.push_back({
                    ._base_url = source->second._url,
                    ._part = source_name,
                    ._path = QString(file._path.c_str()),
                    ._sha1 = file._sha1,
            });
        }
    }

    connect(&_files_checker, &FilesChecker::fileCheckedOk, this, &FilesDownloader::onFileCheckedOk);
    connect(&_files_checker, &FilesChecker::fileCheckFailed, this, &FilesDownloader::onFileCheckFailed);
    connect(&_files_checker, &FilesChecker::error, this, &FilesDownloader::onFileCheckError);
    connect(&_files_checker, &FilesChecker::completed, this, &FilesDownloader::onFileCheckCompleted);

    _files_checker.setFilesToCheck(std::move(expected_files));
}

void FilesDownloader::onFileCheckedOk(ExpectedFile file)
{
    _ctx.logVerbose(3, "File {} is up to date", file._path);
}

void FilesDownloader::onFileCheckFailed(ExpectedFile file, FilesChecker::Reason reason)
{
    _ctx.logVerbose(2, "File {} will be downloaded: {}", file._path, reasonToString(reason));
    _download_queue.emplace_back(std::move(file));
    tryStartDownload();
}

void FilesDownloader::onFileCheckError(std::string message)
{
    emit downloadError(message);
}

void FilesDownloader::onFileCheckCompleted()
{
    tryComplete();
}

void FilesDownloader::onDownloadError(std::string message)
{
    emit downloadError(message);
}

void FilesDownloader::onDownloadSuccess(QByteArray data, bool is_binary)
{
    FileDownloader *downloader = qobject_cast<FileDownloader *>(sender());
    if (downloader == nullptr) {
        throw std::runtime_error("Expected FileDownloader in onDownloadSuccess handler");
    }

    auto it = std::find_if(begin(_downloaders), end(_downloaders),
                           [downloader](const auto &e) { return e.first.get() == downloader; });
    if (it == end(_downloaders)) {
        throw std::runtime_error("Unexpected FileDownloader instance in onDownloadSuccess");
    }

    auto &[_, file] = *it;

    QString file_path = _ctx._scratch.filePath(file._part + QDir::separator() + file._path);
    QFileInfo file_info(file_path);
    QDir file_dir = file_info.dir();
    if (not file_dir.exists()) {
        if (not file_dir.mkpath(".")) {
            emit downloadError(
                    fmt::format("Failed to create directories leading to {}", file_path));
            return;
        }
    }

    QFile fd(file_path);
    if (not fd.open(QFile::Truncate | QFile::WriteOnly)) {
        emit downloadError(
                fmt::format("Failed to create file {}: {}", file_path, fd.errorString()));
        return;
    }
    if (not writeFile(fd, data, is_binary)) {
        emit downloadError(fmt::format("Failed to write {}B to file {}: {}", data.size(), file_path,
                                       fd.errorString()));
        return;
    }
    fd.close();

    if (not fd.open(QFile::ReadOnly)) {
        emit downloadError(fmt::format("Failed to open file {} after writing it: {}", file_path,
                                       fd.errorString()));
        return;
    }
    QByteArray dl_hash = hashFile(fd);
    fd.close();
    if (dl_hash.size() == 0) {
        emit downloadError(fmt::format("Failed to compute checksum of downloaded file {}: {}",
                                       file_path, fd.errorString()));
        return;
    }
    if (dl_hash != file._sha1) {
        emit downloadError(fmt::format("Failed to download {}: checksum mismatch", file_path));
        return;
    }

    _downloaded_files.append(DownloadedFile {
            ._part = file._part,
            ._sub_path = file._path,
            ._scratch_path = file_path,
    });

    _ctx.logVerbose(2, "Done downloading {}", file_path);
    _downloaders.erase(it);

    tryStartDownload();
}

void FilesDownloader::tryStartDownload()
{
    if (_download_queue.empty()) {
        tryComplete();
        return;
    }

    if (_downloaders.size() >= MaxConcurrentDownloads) {
        return;
    }

    auto file = std::move(_download_queue.back());
    _download_queue.pop_back();

    QString url_str = file._base_url.toString();
    if (not url_str.endsWith("/")) {
        url_str += "/";
    }
    url_str += file._path;

    QUrl url(url_str);
    _ctx.logVerbose(1, "Downloading {}", url_str);
    auto &[downloader, _] = _downloaders.emplace_back(
            FileDownloaderPtr(new FileDownloader(this, _ctx._qnam, url)), std::move(file));

    connect(downloader.get(), &FileDownloader::downloadFail, this,
            &FilesDownloader::onDownloadError);
    connect(downloader.get(), &FileDownloader::downloadSuccess, this,
            &FilesDownloader::onDownloadSuccess);
}

void FilesDownloader::tryComplete()
{
    if (_download_queue.empty() && _downloaders.empty() && _files_checker.isComplete()) {
        emit allFilesDownloaded(std::move(_downloaded_files));
    }
}
