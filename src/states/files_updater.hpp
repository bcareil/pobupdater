#ifndef FILESUPDATER_HPP
#define FILESUPDATER_HPP

#include <QFutureWatcher>
#include <QList>
#include <QObject>

#include "context.hpp"
#include "utils/expected_file.hpp"

struct FileUpdateResult;

class FilesUpdater : public QObject
{
    Q_OBJECT
public:
    explicit FilesUpdater(QObject *parent, Context &ctx, QList<DownloadedFile> files);
    ~FilesUpdater();

signals:
    void allFilesUpdated(QList<UpdatedFile> files);
    void updateError(std::string message);

private Q_SLOTS:
    void onFileUpdatedAt(int);
    void onFinished();

private:
    Context &_ctx;
    bool _cancelled = false;
    QFutureWatcher<FileUpdateResult> _watcher;
    QList<DownloadedFile> _downloaded_files;
};

#endif // FILESUPDATER_HPP
