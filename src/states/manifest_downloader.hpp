#ifndef MANIFESTDOWNLOADER_H
#define MANIFESTDOWNLOADER_H

#include <QObject>

#include "../context.hpp"
#include "../utils/file_downloader.hpp"
#include "../utils/manifest_content.hpp"

class ManifestDownloader : public QObject
{
    Q_OBJECT
public:
    explicit ManifestDownloader(QObject *parent, Context &ctx, const QUrl &manifest_url);

signals:
    void manifestDownloaded(ManifestContent manifest);
    void downloadError(std::string message);

private Q_SLOTS:
    void onManifestDownloaded(QByteArray data, bool is_binary);
    void onDownloadError(std::string message);

private:
    Context &_ctx;
    FileDownloader _downloader;
};

#endif // MANIFESTDOWNLOADER_H
