#include "files_updater.hpp"

#include <variant>

#include <QtConcurrent>

#include "fmt-qt/string.hpp"

struct FileUpdateResult
{
    enum class Error {
        InvalidPartName,
        SourceFileMissing,
        FailedToCreateDirectory,
        FailedToMoveFile,
    };

    std::variant<Error, UpdatedFile> _result;
};

namespace {

FileUpdateResult updateFile(const DownloadedFile &file)
{
    QString prefix = file._part + "dst";
    QDir dst_root(prefix + ":/");
    QFileInfo file_info(dst_root, file._sub_path);
    QDir dst_dir = file_info.dir();
    if (not dst_dir.mkpath(".")) {
        return { FileUpdateResult::Error::FailedToCreateDirectory };
    }

    QFile fd(file._scratch_path);
    if (not fd.exists()) {
        return { FileUpdateResult::Error::SourceFileMissing };
    }

    QString dst_full_path = file_info.absoluteFilePath();
    if (QFile::exists(dst_full_path)) {
        QFile::remove(dst_full_path);
    }

    if (not fd.rename(dst_full_path)) {
        return { FileUpdateResult::Error::FailedToMoveFile };
    }

    return { UpdatedFile {
            ._part = file._part,
            ._sub_path = file._sub_path,
            ._full_path = dst_full_path,
    } };
}

std::string fileUpdateErrorToString(FileUpdateResult::Error e, const DownloadedFile &f)
{
    switch (e) {
    case FileUpdateResult::Error::FailedToCreateDirectory:
        return fmt::format("Failed to create the target directory for {}", f._sub_path);
    case FileUpdateResult::Error::FailedToMoveFile:
        return fmt::format("Failed to move {} from {} to its target directory", f._sub_path,
                           f._scratch_path);
    case FileUpdateResult::Error::InvalidPartName:
        return fmt::format("Invalid part name '{}'", f._part);
    case FileUpdateResult::Error::SourceFileMissing:
        return fmt::format("Failed to move {} from {} as it does not exist anymore", f._sub_path,
                           f._scratch_path);
    }
    return fmt::format("Unknown error while processing {}", f._sub_path);
}

} // end of anonymous namespace

FilesUpdater::FilesUpdater(QObject *parent, Context &ctx, QList<DownloadedFile> files)
    : QObject(parent), _ctx(ctx), _watcher(this), _downloaded_files(std::move(files))
{
    connect(&_watcher, SIGNAL(resultReadyAt(int)), this, SLOT(onFileUpdatedAt(int)));
    connect(&_watcher, SIGNAL(finished()), this, SLOT(onFinished()));

    auto future = QtConcurrent::mapped(_downloaded_files, updateFile);
    _watcher.setFuture(future);
}

FilesUpdater::~FilesUpdater() { }

void FilesUpdater::onFileUpdatedAt(int i)
{
    if (_cancelled) {
        return;
    }
    auto result = _watcher.resultAt(i);
    auto perror = std::get_if<FileUpdateResult::Error>(&result._result);
    if (perror) {
        _cancelled = true;
        _watcher.cancel();
        emit updateError(fileUpdateErrorToString(*perror, _downloaded_files[i]));
    }
}

void FilesUpdater::onFinished()
{
    if (_cancelled) {
        return;
    }

    QList<UpdatedFile> updated_files;
    updated_files.reserve(_downloaded_files.size());
    for (int i = 0; i < _downloaded_files.size(); ++i) {
        auto result = _watcher.resultAt(i);
        updated_files.push_back(std::get<UpdatedFile>(result._result));
    }

    emit allFilesUpdated(std::move(updated_files));
}
