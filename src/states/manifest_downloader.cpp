#include "manifest_downloader.hpp"

#include "../utils/manifest_parser.hpp"
#include "../fmt-qt/string.hpp"

ManifestDownloader::ManifestDownloader(QObject *parent, Context &ctx, const QUrl &manifest_url)
    : QObject(parent), _ctx(ctx), _downloader(this, ctx._qnam, manifest_url)
{
    connect(&_downloader, &FileDownloader::downloadSuccess, this,
            &ManifestDownloader::onManifestDownloaded);
    connect(&_downloader, &FileDownloader::downloadFail, this,
            &ManifestDownloader::onDownloadError);
}

void ManifestDownloader::onManifestDownloaded(QByteArray data, bool is_binary)
{
    (void)is_binary;

    auto file_path = _ctx._scratch.filePath("manifest.xml");
    QFile file(file_path);
    if (file.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        auto written = file.write(data);
        if (file.error() || written != data.size() || not file.flush()) {
            emit downloadError("Failed to write manifest to disk");
            return;
        }
        file.close();
    } else {
        emit downloadError(fmt::format("Failed to create manifest file at {}", file_path));
    }

    try {
        auto content = ManifestParser::parse(data);
        emit manifestDownloaded(std::move(content));
    } catch (const std::runtime_error &ex) {
        emit downloadError(ex.what());
    }
}

void ManifestDownloader::onDownloadError(std::string message)
{
    emit downloadError(message);
}
