#pragma once

#include <memory>

#include <QObject>
#include <QUrl>
#include <QByteArray>

QT_FORWARD_DECLARE_CLASS(QNetworkReply);
QT_FORWARD_DECLARE_CLASS(QNetworkAccessManager);

class FileDownloader : public QObject
{
    Q_OBJECT
public:
    static constexpr unsigned MaxRedirectCount = 6;
    static constexpr unsigned MaxFileSize = 100 * 1024 * 1024;

public:
    FileDownloader(QObject *parent, QNetworkAccessManager &qnam, const QUrl &url);
    ~FileDownloader();

signals:
    void downloadSuccess(QByteArray data, bool is_binary);
    void downloadFail(std::string message);

private Q_SLOTS:
    void onFinished();
    void onReadyRead();

private:
    void startRequest(const QUrl &url);

private:
    QNetworkAccessManager &_qnam;
    QUrl _url;
    std::unique_ptr<QNetworkReply> _reply;
    QByteArray _data;
    unsigned _redirectCount = 0;
};
