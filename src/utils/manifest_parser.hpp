#ifndef MANIFESTPARSER_H
#define MANIFESTPARSER_H

#include <map>
#include <string>
#include <vector>

#include <QUrl>

#include "manifest_content.hpp"

struct ManifestParseError : public std::runtime_error
{
    ManifestParseError(const std::string &msg);
};

class ManifestParser
{
public:
    static ManifestContent parse(const QByteArray &manifest_content);
};

#endif // MANIFESTPARSER_H
