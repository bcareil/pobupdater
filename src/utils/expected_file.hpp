#pragma once

#include <QByteArray>
#include <QString>
#include <QUrl>

struct ExpectedFile
{
    QUrl _base_url;
    QString _part;
    QString _path;
    QByteArray _sha1;
};

struct DownloadedFile
{
    QString _part;
    QString _sub_path;
    QString _scratch_path;
};

struct UpdatedFile
{
    QString _part;
    QString _sub_path;
    QString _full_path;
};
