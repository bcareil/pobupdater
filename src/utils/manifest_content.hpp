#pragma once

#include <map>
#include <string>

#include <QByteArray>
#include <QUrl>

struct ManifestContent
{
    struct FileEntry
    {
        std::string _path;
        QByteArray _sha1;
        std::string _platform; // may be empty
    };

    struct SourceEntry
    {
        QUrl _url;
        std::string _platform;
    };

    std::string _version;
    std::map<std::string, SourceEntry> _sources;
    std::map<std::string, std::vector<FileEntry>> _files;
};
