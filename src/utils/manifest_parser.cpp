#include "manifest_parser.hpp"

#include <set>

#include <QDir>
#include <QXmlStreamReader>

#include <fmt/format.h>

#include "../fmt-qt/string.hpp"

namespace {

void consistencyChecks(const ManifestContent &content)
{
    std::vector<std::string> sources_to_check = {
        "default",
        "program",
        "tree",
    };

    std::set<std::string> files;
    for (const auto &src : sources_to_check) {
        auto it = content._files.find(src);
        if (it == end(content._files)) {
            throw ManifestParseError(fmt::format("Missing expected source {}", src));
        }
        for (const auto &f : it->second) {
            auto [it, inserted] = files.insert(f._path);
            if (not inserted) {
                throw ManifestParseError(fmt::format("Duplicate entry for file {}", f._path));
            }
        }
    }
}

} // end of anonymous namespace

ManifestParseError::ManifestParseError(const std::string &msg)
    : std::runtime_error(fmt::format("Error parsing manifest: {}", msg))
{
}

ManifestContent ManifestParser::parse(const QByteArray &manifest_content)
{
    enum class State {
        Start,
        InPobVersion,
        InSubTag,
        End,
    };

    State state = State::Start;
    ManifestContent content;
    QXmlStreamReader reader(manifest_content);

    const QString tag_file = "File";
    const QString tag_version = "Version";
    const QString tag_source = "Source";
    const QString tag_pob_version = "PoBVersion";
    const QString attr_number = "number";
    const QString attr_part = "part";
    const QString attr_url = "url";
    const QString attr_platform = "platform";
    const QString attr_name = "name";
    const QString attr_sha1 = "sha1";

    while (!reader.atEnd()) {
        auto token_type = reader.readNext();
        switch (token_type) {
            // the following are ignored
        case QXmlStreamReader::NoToken:
        case QXmlStreamReader::Comment:
        case QXmlStreamReader::DTD:
        case QXmlStreamReader::StartDocument:
        case QXmlStreamReader::EndDocument:
            break;
            // the following are unexpected and not handled
        case QXmlStreamReader::EntityReference:
        case QXmlStreamReader::ProcessingInstruction:
            // the following are likelly errors
        case QXmlStreamReader::Invalid:
            throw ManifestParseError(reader.errorString().toStdString());
        case QXmlStreamReader::Characters:
            if (not reader.isWhitespace()) {
                throw ManifestParseError("Unexpected token");
            }
            break;
        case QXmlStreamReader::StartElement:
            switch (state) {
            case State::Start:
                if (reader.name().compare(tag_pob_version, Qt::CaseInsensitive) == 0) {
                    state = State::InPobVersion;
                } else {
                    throw ManifestParseError(
                            fmt::format("Expected tag PoBVersion, got tag {}", reader.name()));
                }
                break;
            case State::InPobVersion:
                state = State::InSubTag;
                if (reader.name().compare(tag_file, Qt::CaseInsensitive) == 0) {
                    ManifestContent::FileEntry file;
                    std::string source;
                    QString path;
                    auto attrs = reader.attributes();
                    for (const auto &attr : attrs) {
                        if (attr.name() == attr_name) {
                            path = attr.value().toString();
                        } else if (attr.name() == attr_part) {
                            source = attr.value().toString().toStdString();
                        } else if (attr.name() == attr_platform) {
                            file._platform = attr.value().toString().toStdString();
                        } else if (attr.name() == attr_sha1) {
                            file._sha1 = QByteArray::fromHex(attr.value().toUtf8());
                        }
                    }
                    if (path.size() == 0) {
                        throw ManifestParseError(
                                fmt::format("{} is missing attribute {}", tag_file, attr_name));
                    } else if (file._sha1.size() != 20) {
                        throw ManifestParseError(fmt::format(
                                "{} is missing or has invalid attribute {}", tag_file, attr_sha1));
                    } else if (source.size() == 0) {
                        throw ManifestParseError(
                                fmt::format("{} is missing attribute {}", tag_file, attr_part));
                    }
                    QDir test_path(path);

                    file._path = path.toStdString();
                    // NOTE: platform attribute is optional
                    content._files[source].emplace_back(std::move(file));
                } else if (reader.name().compare(tag_source, Qt::CaseInsensitive) == 0) {
                    ManifestContent::SourceEntry entry;
                    std::string source;
                    for (const auto &attr : reader.attributes()) {
                        if (attr.name() == attr_part) {
                            source = attr.value().toString().toStdString();
                        } else if (attr.name() == attr_platform) {
                            entry._platform = attr.value().toString().toStdString();
                        } else if (attr.name() == attr_url) {
                            QString url = attr.value().toString();
                            url.replace("{branch}", "master");
                            entry._url = QUrl(url);
                        }
                    }
                    if (source.empty()) {
                        throw ManifestParseError(
                                fmt::format("{} is missing attribute {}", tag_source, attr_part));
                    }
                    if (not entry._url.isValid()) {
                        throw ManifestParseError(fmt::format(
                                "{} is missing or has invalid attribute {}", tag_source, attr_url));
                    }
                    // NOTE: platform attribute is optional
                    if (content._sources.count(source) != 0) {
                        throw ManifestParseError(
                                fmt::format("Duplicate {} for {}", tag_source, source));
                    }
                    content._sources[source] = std::move(entry);
                } else if (reader.name().compare(tag_version, Qt::CaseInsensitive) == 0) {
                    std::string version;
                    for (const auto &attr : reader.attributes()) {
                        if (attr.name() == attr_number) {
                            version = attr.value().toString().toStdString();
                        }
                    }
                    if (version.empty()) {
                        throw ManifestParseError(fmt::format("{} is missing attribute {}",
                                                             tag_version, attr_number));
                    }
                    if (not content._version.empty()) {
                        throw ManifestParseError(
                                fmt::format("Duplicate entry for {}", tag_version));
                    }
                    content._version = version;
                } else {
                    throw ManifestParseError(fmt::format("Expected one of {}, {} or {}, got tag {}",
                                                         tag_file, tag_source, tag_version,
                                                         reader.name()));
                }
                break;
            case State::InSubTag:
                throw ManifestParseError(
                        fmt::format("Expected end of tag, got tag {}", reader.name()));
            case State::End:
                throw ManifestParseError(
                        fmt::format("Expected end of file, got tag {}", reader.name()));
            }
            break;
        case QXmlStreamReader::EndElement:
            switch (state) {
            case State::Start:
                throw ManifestParseError("Empty manifest file");
            case State::InPobVersion:
                state = State::End;
                break;
            case State::InSubTag:
                state = State::InPobVersion;
                break;
            case State::End:
                // would imply a malformed xml, which should be reported by reader.error()
                throw ManifestParseError("Unexpected closing tag");
            }
            break;
        }
    }
    if (reader.hasError()) {
        throw ManifestParseError(reader.errorString().toStdString());
    }
    consistencyChecks(content);
    return content;
}
