#include "file_downloader.hpp"

#include <QNetworkReply>
#include <QNetworkAccessManager>

#include "fmt-qt/string.hpp"

namespace {

bool isBinary(const QVariant &content_type)
{
    QString str = content_type.toString();
    if (str.startsWith("text/")) {
        return false;
    }
    return true;
}

}

FileDownloader::FileDownloader(QObject *parent, QNetworkAccessManager &qnam, const QUrl &url)
    : QObject(parent), _qnam(qnam)
{
    startRequest(url);
}

FileDownloader::~FileDownloader() { }

void FileDownloader::onFinished()
{
    if (_reply->error()) {
        emit downloadFail(
                fmt::format("Failed to download {}: {}", _url.toString(), _reply->errorString()));
        return;
    }

    QVariant content_type = _reply->header(QNetworkRequest::ContentTypeHeader);
    bool is_binary = isBinary(content_type);

    const QVariant redirectionTarget =
            _reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    _reply.release()->deleteLater();
    if (redirectionTarget.isNull()) {
        emit downloadSuccess(std::move(_data), is_binary);
    } else if (_redirectCount++ < MaxRedirectCount) {
        auto new_url = _url.resolved(redirectionTarget.toUrl());
        if (new_url.scheme() != "https") {
            emit downloadFail(
                    fmt::format("Failed to download {}: redirection to insecure protocol {}: {}",
                                _url.toString(), new_url.scheme(), new_url.toString()));
            return;
        }
        startRequest(new_url);
    } else {
        emit downloadFail(fmt::format("Failed to download {}: exceeded redirection count ({})",
                                      _url.toString(), MaxRedirectCount));
    }
}

void FileDownloader::onReadyRead()
{
    QByteArray data = _reply->readAll();
    _data.append(data);
    if (_data.size() > MaxFileSize) {
        _data = QByteArray();
        _reply->abort();
        throw std::runtime_error("Download exceeded max file size");
    }
}

void FileDownloader::startRequest(const QUrl &url)
{
    _reply.reset(_qnam.get(QNetworkRequest(url)));
    if (_reply == nullptr) {
        throw std::runtime_error("Failed to create network request");
    }
    connect(_reply.get(), SIGNAL(finished()), this, SLOT(onFinished()));
    connect(_reply.get(), SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}
