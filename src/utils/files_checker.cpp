#include "files_checker.hpp"

#include <QtConcurrent>
#include <QCryptographicHash>
#include <QFile>
#include <QScopedPointerObjectDeleteLater>

#include "../states/files_downloader.hpp"

namespace {

FileHashResult hashFile(const ExpectedFile &file)
{
    QString file_path = file._part + "src:/" + file._path;
    QFile fd(file_path);
    if (not fd.exists()) {
        return { ._error = FileHashResult::Error::FileMissing };
    }
    if (not fd.open(QFile::ReadOnly)) {
        return { ._error = FileHashResult::Error::OpenError };
    }

    QCryptographicHash hash(QCryptographicHash::Sha1);
    std::array<char, 4096> buffer;
    while (not fd.atEnd()) {
        qint64 read = fd.read(buffer.data(), buffer.size());
        if (read <= 0 || fd.error()) {
            return { ._error = FileHashResult::Error::ReadError };
        }
        hash.addData(buffer.data(), read);
    }

    return { ._hash = hash.result() };
}

} // end of anonymous namespace

using FilePtr = std::unique_ptr<QFile, DeleteLater>;

struct FilesChecker::PendingFile
{
    QFutureWatcher<QByteArray> _watcher;
    ExpectedFile _info;
};

FilesChecker::FilesChecker(QObject *parent, Context &ctx)
    : QObject(parent), _ctx(ctx), _watcher(this)
{
    connect(&_watcher, SIGNAL(resultReadyAt(int)), this, SLOT(onFileHashedAt(int)));
    connect(&_watcher, SIGNAL(finished()), this, SLOT(onFinished()));
}

FilesChecker::~FilesChecker() { }

void FilesChecker::setFilesToCheck(std::vector<ExpectedFile> &&files)
{
    if (_watcher.isRunning() || _watcher.isPaused()) {
        _watcher.cancel();
        _watcher.waitForFinished();
    }
    _files_to_check = std::move(files);
    auto future = QtConcurrent::mapped(_files_to_check, hashFile);
    _watcher.setFuture(future);
}

bool FilesChecker::isComplete() const
{
    return _watcher.isFinished() || _watcher.isCanceled();
}

void FilesChecker::onFileHashedAt(int idx)
{
    auto &file = _files_to_check.at(idx);
    auto result = _watcher.resultAt(idx);

    if (result._error.has_value()) {
        switch (result._error.value()) {
        case FileHashResult::Error::FileMissing:
            emit fileCheckFailed(std::move(file), Reason::FileMissing);
            return;
        case FileHashResult::Error::OpenError:
            emit error(fmt::format("Failed to open {}", file._path));
            return;
        case FileHashResult::Error::ReadError:
            emit error(fmt::format("Read error while hashing file {}", file._path));
            return;
        }
        throw std::runtime_error("Unexpected error enum value");
    } else {
        if (result._hash == file._sha1) {
            emit fileCheckedOk(std::move(file));
        } else {
            emit fileCheckFailed(std::move(file), Reason::ChecksumMismatch);
        }
    }
}

void FilesChecker::onFinished()
{
    emit completed();
}
