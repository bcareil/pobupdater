#ifndef FILESCHECKER_HPP
#define FILESCHECKER_HPP

#include <vector>

#include <QObject>
#include <QList>
#include <QTimer>

#include "../context.hpp"

#include <QFutureWatcher>

#include "expected_file.hpp"

struct FileHashResult
{
    enum class Error {
        FileMissing,
        ReadError,
        OpenError,
    };

    QByteArray _hash;
    std::optional<Error> _error;
};

class FilesChecker : public QObject
{
    Q_OBJECT

    struct PendingFile;

public:
    static constexpr size_t MaxConcurrentChecks = 12;

    enum class Reason {
        FileMissing,
        ChecksumMismatch,
    };

public:
    explicit FilesChecker(QObject *parent, Context &ctx);
    ~FilesChecker();

    void setFilesToCheck(std::vector<ExpectedFile> &&files);
    bool isComplete() const;

signals:
    void fileCheckedOk(ExpectedFile file);
    void fileCheckFailed(ExpectedFile file, Reason reason);
    void error(std::string message);
    void completed();

private Q_SLOTS:
    void onFileHashedAt(int);
    void onFinished();

private:
    bool updateEntry(PendingFile &entry);
    void tryComplete();

private:
    Context &_ctx;
    QFutureWatcher<FileHashResult> _watcher;
    std::vector<ExpectedFile> _files_to_check;
};

#endif // FILESCHECKER_HPP
