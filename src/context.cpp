#include "context.hpp"

#include <iostream>

void Context::printErr(const std::string &s)
{
    std::cerr << s;
    if (not s.empty() && s.back() == '\n') {
        std::cerr << "\n";
    }
}
