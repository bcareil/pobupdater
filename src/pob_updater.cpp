#include "pob_updater.hpp"

#include <iostream>

#include "fmt-qt/string.hpp"

#include "states/manifest_downloader.hpp"
#include "states/files_downloader.hpp"
#include "states/files_updater.hpp"

namespace {

void validatePartName(const QString &part)
{
    if (not part.isLower()) {
        throw std::runtime_error(fmt::format("Invalid part name: {}", part));
    }
}

} // end of anonymous namespace

PobUpdater::PobUpdater(Config config)
    : _ctx { ._qnam = QNetworkAccessManager(this),
             ._scratch = config._scratch_directory,
             ._parts = config._parts.keys(),
             ._verbose_level = config._verbose,
             ._dry_run = config._dry_run }
{
    for (auto it = config._parts.constKeyValueBegin(), end = config._parts.constKeyValueEnd();
         it != end; ++it) {
        const auto &[part, dirs] = *it;

        validatePartName(part);

        QStringList src_dirs;
        QStringList dst_dirs;

        dst_dirs << dirs._destination.absolutePath();
        for (const auto &src_dir : dirs._sources) {
            src_dirs << src_dir.absolutePath();
        }

        QString src_prefix = part + "src";
        QDir::setSearchPaths(src_prefix, src_dirs);

        QString dst_prefix = part + "dst";
        QDir::setSearchPaths(dst_prefix, dst_dirs);
    }
    _manifest_downloader = std::make_unique<ManifestDownloader>(this, _ctx, config._manifest_url);

    connect(_manifest_downloader.get(), &ManifestDownloader::manifestDownloaded, this,
            &PobUpdater::onManifestDownloaded);
    connect(_manifest_downloader.get(), &ManifestDownloader::downloadError, this,
            &PobUpdater::onError);
}

PobUpdater::~PobUpdater() { }

void PobUpdater::onManifestDownloaded(ManifestContent manifest)
{
    _manifest_downloader.release()->deleteLater();
    _files_downloader = std::make_unique<FilesDownloader>(this, _ctx, std::move(manifest));
    connect(_files_downloader.get(), &FilesDownloader::allFilesDownloaded, this,
            &PobUpdater::onAllFilesDownloaded);
    connect(_files_downloader.get(), &FilesDownloader::downloadError, this, &PobUpdater::onError);
}

void PobUpdater::onAllFilesDownloaded(QList<DownloadedFile> files)
{
    _files_downloader.release()->deleteLater();
    _files_updater = std::make_unique<FilesUpdater>(this, _ctx, std::move(files));
    connect(_files_updater.get(), &FilesUpdater::allFilesUpdated, this,
            &PobUpdater::onAllFilesUpdated);
    connect(_files_updater.get(), &FilesUpdater::updateError, this, &PobUpdater::onError);
}

void PobUpdater::onAllFilesUpdated(QList<UpdatedFile> files)
{
    emit complete(std::move(files));
}

void PobUpdater::onError(std::string message)
{
    emit error(std::move(message));
}
