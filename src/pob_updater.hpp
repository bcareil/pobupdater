#ifndef POBUPDATER_H
#define POBUPDATER_H

#include <QDir>
#include <QList>
#include <QMap>
#include <QObject>
#include <QString>
#include <QUrl>

#include "utils/manifest_content.hpp"
#include "utils/expected_file.hpp"
#include "context.hpp"

class ManifestDownloader;
class FilesDownloader;
class FilesUpdater;

class PobUpdater : public QObject
{
    Q_OBJECT
public:
    struct Config
    {
        struct Part
        {
            QList<QDir> _sources;
            QDir _destination;
        };

        /// List of parts to evaluate: where to find original files and where to put downloaded ones
        QMap<QString, Part> _parts;
        /// Initial download directory
        QDir _scratch_directory;
        /// URL of the update manifest
        QUrl _manifest_url;
        /// Whether to do more than just downloading the manfiest and looking at which file should
        /// be downloaded
        bool _dry_run = false;

        unsigned _verbose = 0;
    };

public:
    explicit PobUpdater(Config config);
    ~PobUpdater();

signals:
    void error(std::string message);
    void complete(QList<UpdatedFile> files);

private Q_SLOTS:
    void onManifestDownloaded(ManifestContent manifest);
    void onAllFilesDownloaded(QList<DownloadedFile> files);
    void onAllFilesUpdated(QList<UpdatedFile> files);
    void onError(std::string message);

private:
    Context _ctx;
    std::unique_ptr<ManifestDownloader> _manifest_downloader;
    std::unique_ptr<FilesDownloader> _files_downloader;
    std::unique_ptr<FilesUpdater> _files_updater;
};

#endif // POBUPDATER_H
