#include <iostream>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>
#include <QList>
#include <QUrl>

#include <fmt/format.h>

#include "fmt-qt/string.hpp"

#include "pob_updater.hpp"

namespace {

struct PartOptions
{
    QCommandLineOption _src;
    QCommandLineOption _dst;
};

} // end of anonymous namespace

struct MissingArgumentError : public std::runtime_error
{
    MissingArgumentError(const QString &arg_name)
        : std::runtime_error(fmt::format("Missing or empty value for argument {}", arg_name))
    {
    }
};

struct InvalidArgumentError : public std::runtime_error
{
    InvalidArgumentError(const QString &arg_name, const QString &value, std::string_view desc)
        : std::runtime_error(fmt::format("Invalid argument {} ('{}'): {}", arg_name, value, desc))
    {
    }
};

QUrl parse_cli_url(const QString &in_url, const QString &arg_name)
{
    if (in_url.size() == 0) {
        throw MissingArgumentError(arg_name);
    }
    QUrl url(in_url);
    if (not url.isValid()) {
        throw InvalidArgumentError(arg_name, in_url, "expected an URL");
    }
    return url;
}

QDir parse_cli_dir(const QString &in_dir, const QString &arg_name)
{
    if (in_dir.size() == 0) {
        throw MissingArgumentError(arg_name);
    }

    QDir dir(in_dir);
    if (not dir.exists()) {
        throw InvalidArgumentError(arg_name, in_dir, "expected a path to a directory");
    }
    return dir;
}

PobUpdater::Config parse_cli(const QCoreApplication &app)
{
    PobUpdater::Config config;
    QCommandLineParser p;

    p.setApplicationDescription(
            "Download updates for a set of file listed in a manifest.\n\n"
            "The manifest can be fetch from a remote location. Files will be first looked up in "
            "the source directories. If their are missing or their checksum do not match, they "
            "will be downloaded into the scratch directory. If all files are downloaded correctly, "
            "they will be moved into the destination directory, potentially overwritting files "
            "already present there.");
    p.addHelpOption();

    p.addPositionalArgument("source", "Directories where original files will be looked up.");
    p.addOptions({
            { "manifest", "URL to the manifest file", "URL" },
            { "scratch",
              "Directory to use as a temporary directory, where files will be downloaded.", "DIR" },
            { { "n", "dry-run" },
              "Do not download any file but the manifest. List the files not up to date with "
              "regards to the manifest." },
            { { "v", "verbose" }, "Be more verbose", "LEVEL" },
    });

    std::map<std::string, PartOptions> parts_options;
    for (const auto &s : { "tree", "default", "program" }) {
        std::string dst_arg = fmt::format("dst-{}", s);
        std::string dst_desc = fmt::format(
                "Where to copy files for part \"{}\" after they have been downloaded", s);
        std::string src_arg = fmt::format("src-{}", s);
        std::string src_desc = fmt::format("Where to look for files from part \"{}\"", s);
        auto [it, inserted] = parts_options.insert(std::make_pair(
                s,
                PartOptions {
                        ._src = QCommandLineOption(src_arg.c_str(), src_desc.c_str(), "DIR"),
                        ._dst = QCommandLineOption(dst_arg.c_str(), dst_desc.c_str(), "DIR"),
                }));
        assert(inserted);
        bool ok;
        ok = p.addOption(it->second._src);
        assert(ok);
        ok = p.addOption(it->second._dst);
        assert(ok);
    }

    p.process(app);

    config._dry_run = p.isSet("dry-run");
    config._verbose = p.isSet("verbose");

    QString manifest = p.value("manifest");
    QString scratch = p.value("scratch");
    config._manifest_url = parse_cli_url(manifest, "manifest");
    config._scratch_directory = parse_cli_dir(scratch, "scratch");

    for (const auto &[part, opts] : parts_options) {
        QString dst_str = p.value(opts._dst);
        QDir dst_dir = parse_cli_dir(dst_str, opts._dst.names()[0]);

        QList<QDir> src_dirs;
        QStringList src_strs = p.values(opts._src);
        if (src_strs.empty()) {
            throw MissingArgumentError(opts._src.names()[0]);
        }
        for (const auto &src_str : src_strs) {
            src_dirs.append(parse_cli_dir(src_str, opts._src.names()[0]));
        }

        config._parts.insert(part.c_str(),
                             {
                                     ._sources = std::move(src_dirs),
                                     ._destination = std::move(dst_dir),
                             });
    }

    return config;
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    PobUpdater::Config options;
    try {
        options = parse_cli(app);
    } catch (const std::exception &ex) {
        std::cerr << "Error: " << ex.what() << "\n";
        return 1;
    }

    PobUpdater updater(options);

    app.connect(&updater, &PobUpdater::error, [&app](std::string message) {
        std::cerr << message << "\n";
        std::cerr << "Update aborted\n";
        app.quit();
    });

    app.connect(&updater, &PobUpdater::complete, [&app](QList<UpdatedFile> files) {
        for (const auto &f : files) {
            std::cout << "Updated " << f._sub_path.toStdString() << "\n";
        }
        app.quit();
    });

    return app.exec();
}
